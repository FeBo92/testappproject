﻿using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.MultiTouch;
using System.Drawing;
using UnitConverterApp.Screens.MainSection;

namespace UnitConverterApp.Screens
{
    public class MainSectionsScreen : Header
    {
        public MainSectionsScreen(AppiumDriver<AndroidElement> driver)
            : base(driver)
        {

        }

        private string titleXPath = "//*[@text='Main sections']";
        private string elementXPath = "//*[@text='category']";

        private AndroidElement FindNeedCategory(string categoryName)
        {
            return driver.FindElementByXPath(elementXPath.Replace("category", categoryName));
        }

        private void ClickOnCategory(string categoryName)
        {
            try
            { 
                FindNeedCategory(categoryName).Click();
            }
            catch(WebDriverException)
            {
                SwipeTo();
                FindNeedCategory(categoryName).Click();
            }
        }

        private MainSectionsScreen SwipeTo()
        {
            var element = driver.FindElementByXPath(titleXPath);
            Point point = element.Coordinates.LocationInDom;
            Size size = element.Size;
            new TouchAction(driver)
                .Press(point.X + 600, point.Y + 1500) 
                .Wait(200)
                .MoveTo(point.X + size.Width - 600, point.Y + size.Height - 1500) 
                .Release()
                .Perform();
            return new MainSectionsScreen(driver);
        }

        public Header OpenCategoryInMainSections(string categoryName)
        {
            ClickOnCategory(categoryName);
            return new Header(driver);
        }

        public TimeScreen GetTimeScreen()
        {
            ClickOnCategory("Time");
            return new TimeScreen(driver);
        }

        public WeightScreen GetWeightScreen()
        {
            ClickOnCategory("Weight");
            return new WeightScreen(driver);
        }

        public AreaScreen GetSquareScreen()
        {
            ClickOnCategory("Area");
            return new AreaScreen(driver);
        }

        public PressureScreen GetPreassureScreen()
        {
            ClickOnCategory("Pressure");
            return new PressureScreen(driver);
        }


        public LengthScreen GetLengthScreen()
        {
            ClickOnCategory("Length");
            return new LengthScreen(driver);
        }

        public TemperatureScreen GetTemperatureScreen()
        {
            ClickOnCategory("Temperature");
            return new TemperatureScreen(driver);
        }

        public AngularAccelerationScreen GetAngularAccelerationScreen()
        {
            ClickOnCategory("Angular acceleration");
            return new AngularAccelerationScreen(driver);
        }

        public DensityScreen GetDensityScreen()
        {
            ClickOnCategory("Density");
            return new DensityScreen(driver);
        }

        public NumberScreen GetNumberScreen()
        {
            ClickOnCategory("Number");
            return new NumberScreen(driver);
        }
        
        public BrightnessScreen GetBrightnessScreen()
        {
            ClickOnCategory("Brightness");
            return new BrightnessScreen(driver);
        }
    }
}
