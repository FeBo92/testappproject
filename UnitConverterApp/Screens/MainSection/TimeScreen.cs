﻿using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using UnitConverterApp.Actions;

namespace UnitConverterApp.Screens.MainSection
{
    public class TimeScreen : Header
    {
        public TimeScreen(AppiumDriver<AndroidElement> driver)
          : base(driver)
        {

        }

        public TimeScreen InputValueField(string fieldName, string value)
        {
            new FieldsManager(driver).InputValueField(fieldName, value);
            return this;
        }

        public TimeScreen CheckValueField(string expectedValueField, string fieldName)
        {
            new FieldsManager(driver).CheckValueField(expectedValueField, fieldName);
            return this;
        }
    }
}
