﻿using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using UnitConverterApp.Actions;

namespace UnitConverterApp.Screens.MainSection
{
    public class PressureScreen : Header
    {
        public PressureScreen(AppiumDriver<AndroidElement> driver)
         : base(driver)
        {

        }
        public PressureScreen InputValueField(string fieldName, string value)
        {
            new FieldsManager(driver).InputValueField(fieldName, value);
            return this;
        }

        public PressureScreen CheckValueField(string expectedValueField, string fieldName)
        {
            new FieldsManager(driver).CheckValueField(expectedValueField, fieldName);
            return this;
        }
    }
}
