﻿using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using UnitConverterApp.Actions;


namespace UnitConverterApp.Screens.MainSection
{
    public class LengthScreen : Header
    {
        public LengthScreen(AppiumDriver<AndroidElement> driver)
        : base(driver)
        {

        }

        public LengthScreen InputValueField(string fieldName, string value)
        {
            new FieldsManager(driver).InputValueField(fieldName, value);
            return this;
        }

        public LengthScreen CheckValueField(string expectedValueField, string fieldName)
        {
            new FieldsManager(driver).CheckValueField(expectedValueField, fieldName);
            return this;
        }
    }
}
