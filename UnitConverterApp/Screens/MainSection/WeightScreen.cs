﻿using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using UnitConverterApp.Actions;

namespace UnitConverterApp.Screens.MainSection
{
    public class WeightScreen : Header
    {
        public WeightScreen(AppiumDriver<AndroidElement> driver)
         : base(driver)
        {

        }

        public WeightScreen InputValueField(string fieldName, string value)
        {
            new FieldsManager(driver).InputValueField(fieldName, value);
            return this;
        }

        public WeightScreen CheckValueField(string expectedValueField, string fieldName)
        {
            new FieldsManager(driver).CheckValueField(expectedValueField, fieldName);
            return this;
        }
    }
}

