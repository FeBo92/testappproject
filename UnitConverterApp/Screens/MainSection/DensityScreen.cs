﻿using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using UnitConverterApp.Actions;

namespace UnitConverterApp.Screens.MainSection
{
    public class DensityScreen : Header
    {
        public DensityScreen(AppiumDriver<AndroidElement> driver)
         : base(driver)
        {

        }
        public DensityScreen InputValueField(string fieldName, string value)
        {
            new FieldsManager(driver).InputValueField(fieldName, value);
            return this;
        }

        public DensityScreen CheckValueField(string expectedValueField, string fieldName)
        {
            new FieldsManager(driver).CheckValueField(expectedValueField, fieldName);
            return this;
        }
    }
}