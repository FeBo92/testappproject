﻿using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using UnitConverterApp.Actions;

namespace UnitConverterApp.Screens.MainSection
{
    public class AreaScreen : Header
    {
        public AreaScreen(AppiumDriver<AndroidElement> driver)
         : base(driver)
        {

        }
        public AreaScreen InputValueField(string fieldName, string value)
        {
            new FieldsManager(driver).InputValueField(fieldName, value);
            return this;
        }

        public AreaScreen CheckValueField(string expectedValueField, string fieldName)
        {
            new FieldsManager(driver).CheckValueField(expectedValueField, fieldName);
            return this;
        }
    }
}
