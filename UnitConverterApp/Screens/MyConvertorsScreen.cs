﻿using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.MultiTouch;
using OpenQA.Selenium.Support.UI;
using UnitConverterApp.Actions;

namespace UnitConverterApp.Screens
{
    public class MyConvertorsScreen : Header
    {
        public MyConvertorsScreen(AppiumDriver<AndroidElement> driver)
            : base(driver)
        {

        }

        private string elementXPath = "//*[@text='category']";
        private string newConvertor = "//*[@text='Create']";
        private string convertorNameField = "//*[@id='toolbar_edit']";
        private string addConvertorPlus = "//*[@id='unit_add']";
        private string unit1Field = "//*[@id='unit1_name']";
        private string unit2Field = "//*[@id='unit2_name']";
        private string value1Field = "//*[@id='unit1_value']";
        private string value2Field = "//*[@id='unit2_value']";
        private string canselField = "//*[@text='CANCEL']";
        private string addField = "//*[@text='ADD']";
        private string saveConvertorXPath = "//*[@id='toolbar_save']";
        private string toolbarMenu = "//*[@id='toolbar_menu']";
        private string toolbarItem = "//*[@text='Item']";
        private string button = "//*[@text='DELETE']";


        public MyConvertorsScreen MakeConvertor()
        {
            driver.FindElementByXPath(newConvertor).Click();
            return this;
        }

        public MyConvertorsScreen SetConvertorName(string convertorName)
        {
            driver.FindElementByXPath(convertorNameField).Click();
            driver.FindElementByXPath(convertorNameField).SendKeys(convertorName);
            return this;
        }

        public MyConvertorsScreen AddFields()
        {
            driver.FindElementByXPath(addConvertorPlus).Click();
            return this;
        }

        public MyConvertorsScreen SetUnitName1(string unitName)
        {
            driver.FindElementByXPath(unit1Field).SendKeys(unitName);
            return this;
        }

        public MyConvertorsScreen SetUnitName2(string unitName)
        {
            driver.FindElementByXPath(unit2Field).SendKeys(unitName);
            return this;
        }

        public MyConvertorsScreen SetUnitValue1(string unitValue)
        {
            driver.FindElementByXPath(value1Field).SendKeys(unitValue);
            return this;
        }

        public MyConvertorsScreen SetUnitValue2(string unitValue)
        {
            driver.FindElementByXPath(value2Field).SendKeys(unitValue);
            return this;
        }

        public MyConvertorsScreen SaveFields()
        {
            driver.FindElementByXPath(addField).Click();
            return this;
        }

        public MyConvertorsScreen SaveConvertor()
        {
            driver.FindElementByXPath(saveConvertorXPath).Click();
            return this;
        }
        
        
       

        private AndroidElement FindConvertor(string categoryName)
        {
            return driver.FindElementByXPath(elementXPath.Replace("category", categoryName));
        }

        public MyConvertorsScreen OpenConvertor(string categoryName)
        {

            FindConvertor(categoryName).Click();
            return this;
        }
        public MyConvertorsScreen InputValueField(string fieldName, string value)
        {
            new FieldsManager(driver).InputValueField(fieldName, value);
            return this;
        }

        public MyConvertorsScreen CheckValueField(string expectedValueField, string fieldName)
        {
            new FieldsManager(driver).CheckValueField(expectedValueField, fieldName);
            return this;
        }
        public MyConvertorsScreen DeleteConvertor()
        {
            driver.FindElementByXPath(toolbarMenu).Click();
            driver.FindElementByXPath(toolbarItem.Replace("Item", "Delete")).Click();
            driver.FindElementByXPath(button.Replace("Item", "DELETE")).Click();
            return this;
        }

    }
}