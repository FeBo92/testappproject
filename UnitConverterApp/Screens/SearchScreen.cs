﻿using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;

namespace UnitConverterApp.Screens
{
    public class SearchScreen : Header
    {
        public SearchScreen(AppiumDriver<AndroidElement> driver)
           : base(driver)
        {

        }

        private string elementXPath = "//*[@text='category' and @id='name']";

        public Header ClickOnCategory(string categoryName)
        {
            driver.FindElementByXPath(elementXPath.Replace("category", categoryName));
            return new Header(driver);
        }

        private bool GetAllItems(string categoryName)
        {
            if (driver.FindElementByXPath(elementXPath.Replace("category", categoryName)).Displayed)
            {
                return true;
            }
            return false;
        }

        public SearchScreen CheckFoundCategory(string categoryName)
        {
            Assert.IsTrue(GetAllItems(categoryName));
            return this;
        }

    }
}
