﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;

namespace UnitConverterApp.Screens
{
    public class MainScreen : Header
    {
        public MainScreen(AppiumDriver<AndroidElement> driver)
            : base(driver)
        {

        }

        private string allItemsMenuClass = "android.widget.TextView";
        private string inceptionPathOfElement = "com.kuzmin.konverter:id/menu_";
        private string myConvertorsPath = "//*[@text='My converters']";
        private string myFavorites = "//*[@text='Favourites']";
        private string elementXPath = "//*[@text='category']";
        private string toolbarMenu = "//*[@id='toolbar_menu']";
        private string addToFavorite = "//*[@text='Favourites']";


        private List<AndroidElement> GetAllItems()
        {
            return driver.FindElementsByClassName(allItemsMenuClass).ToList();
        }

        public Header GetNeedScreen(string screenName)
        {
            foreach (AndroidElement screens in GetAllItems())
            {
                if (screens.Text == screenName)
                {
                    screens.Click();
                    break;
                }
            }
            return new Header(driver);
        }

        public MainSectionsScreen GetMainSectionsScreen()
        {
            driver.FindElementById(inceptionPathOfElement + "categories").Click();
            return new MainSectionsScreen(driver);
        }

        public CombineSectionScreen GetCombineSectionScreen()
        {
            driver.FindElementByXPath("//*[@text='Combined sections']").Click();
            return new CombineSectionScreen(driver);
        }

        public MyConvertorsScreen GetMyConvertorsScreen()
        {
            driver.FindElementByXPath(myConvertorsPath).Click();
            return new MyConvertorsScreen(driver);
        }
        
        public MyFavoriteScreen GetMyFavoriteScreen()
        {
            driver.FindElementByXPath(myFavorites).Click();
            return new MyFavoriteScreen(driver);
        }

        public string GetConvertorName(string categoryName)
        {
            return driver.FindElementByXPath(elementXPath.Replace("category", categoryName)).Text;

        }

        public MainScreen CheckConvertor(string expected, string fieldName)
        {
            Assert.AreEqual(expected, GetConvertorName(fieldName));
            return this;
        }

        public MainScreen RemoveFromFavorite(string categoryName)
        {
            FindNeedCategory(categoryName).Click();
            GetToolbarMenu();
            AddToFavorite();
            return this;
        }

        public AndroidElement FindNeedCategory(string categoryName)
        {
            return driver.FindElementByXPath(elementXPath.Replace("category", categoryName));
        }
        public MainScreen GetToolbarMenu()
        {
            driver.FindElementByXPath(toolbarMenu).Click();
            return this;

        }

        public MainScreen AddToFavorite()
        {
            driver.FindElementByXPath(addToFavorite).Click();
            return this;

        }
    }
}
