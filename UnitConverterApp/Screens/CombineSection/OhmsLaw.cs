﻿using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;

namespace UnitConverterApp.Screens
{
    public class OhmsLowScreen : Header
    {
        public OhmsLowScreen(AppiumDriver<AndroidElement> driver)
            : base(driver)
        {

        }

        private string field1 = "//*[@id='edit1']";
        private string field2 = "//*[@id='edit2']";
        private string field3 = "//*[@id='edit3']";

        private AndroidElement InputTextField(string XPath)
        {
            return driver.FindElementByXPath(XPath);
        }
        public OhmsLowScreen InputValueField1(string value)
        {
            driver.FindElementByXPath(field1).Click();
            InputTextField(field1).SendKeys(value);
            return this;
        }

        public OhmsLowScreen InputValueField2(string value)
        {
            driver.FindElementByXPath(field2).Click();
            InputTextField(field2).SendKeys(value);
            return this;
        }


        public string ReadValueField3()
        {
            return driver.FindElementByXPath(field3).Text;
        }

        public OhmsLowScreen CheckValueField3(string expectedValueField)
        {
            Assert.AreEqual(expectedValueField, ReadValueField3().Replace(" ", ""));
            return this;
        }

    }
}
