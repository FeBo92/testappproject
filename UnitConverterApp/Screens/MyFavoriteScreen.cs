﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.MultiTouch;
using System.Drawing;
using UnitConverterApp.Actions;
using UnitConverterApp.Screens.MainSection;

namespace UnitConverterApp.Screens
{
    public class MyFavoriteScreen : Header
    {
        public MyFavoriteScreen(AppiumDriver<AndroidElement> driver)
            : base(driver)
        {

        }

        private string titleXPath = "//*[@text='Main sections']";
        private string elementXPath = "//*[@text='category']";
        private string allConvertors = "//*[@text='All sections']";
        private string toolbarMenu = "//*[@id='toolbar_menu']";
        private string addToFavorite = "//*[@text='Favourites']";

        public AndroidElement FindNeedCategory(string categoryName)
        {
            return driver.FindElementByXPath(elementXPath.Replace("category", categoryName));
        }

        public MyFavoriteScreen ClickOnCategory(string categoryName)
        {
            try
            { 
                FindNeedCategory(categoryName).Click();
                return this;
            }
            catch(WebDriverException)
            {
                SwipeTo();
                FindNeedCategory(categoryName).Click();
                return this;
            }
        }

        private MyFavoriteScreen SwipeTo()
        {
            var element = driver.FindElementByXPath(titleXPath);
            Point point = element.Coordinates.LocationInDom;
            Size size = element.Size;
            new TouchAction(driver)
                .Press(point.X + 600, point.Y + 1500) 
                .Wait(200)
                .MoveTo(point.X + size.Width - 600, point.Y + size.Height - 1500) 
                .Release()
                .Perform();
            return new MyFavoriteScreen(driver);
        }

        public MyFavoriteScreen GetAllConverors()
        {
            driver.FindElementByXPath(allConvertors).Click();
            return this;

        }

        public MyFavoriteScreen GetToolbarMenu()
        {
            driver.FindElementByXPath(toolbarMenu).Click();
            return this;

        }

        public MyFavoriteScreen AddToFavorite()
        {
            driver.FindElementByXPath(addToFavorite).Click();
            return this;

        }
        public string GetConvertorName(string categoryName)
        {
            return driver.FindElementByXPath(elementXPath.Replace("category", categoryName)).Text;
            
        }

        public MyFavoriteScreen CheckConvertor(string expected, string fieldName)
        {
            Assert.AreEqual(expected, GetConvertorName(fieldName));
            return this;
        }

        
    }
}
