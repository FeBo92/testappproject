﻿using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;

namespace UnitConverterApp.Screens
{
    public class Header : BaseScreen
    {
        public Header(AppiumDriver<AndroidElement> driver)
            : base(driver)
        {

        }

        private string screenTitleXPath = "//*[@id='toolbar_title']";
        private string tooLbarButtonBackXPath = "//*[@id='toolbar_back']";
        private string toolbarSearchOnXPath = "//*[@id='toolbar_search']";
        private string toolbarSearchTextXPath = "//*[@id='toolbar_edit']";
        private string toolbarSearchOffXPath = "//*[@id='toolbar_clear']";
        

        private string GetScreenTitle()
        {
            return driver.FindElementByXPath(screenTitleXPath).Text;
        }

        private void OpenSearchField()
        {
            driver.FindElementByXPath(toolbarSearchOnXPath).Click();
        }

        public MainScreen ClickBack()
        {
            driver.FindElementByXPath(tooLbarButtonBackXPath).Click();
            return new MainScreen(driver);
        }

        public MainScreen GetMainScreen()
        {
            driver.FindElementByXPath(tooLbarButtonBackXPath).Click();
            return new MainScreen(driver);
        }

        public CombineSectionScreen GetCombineSectionScreen()
        {
            driver.FindElementByXPath(tooLbarButtonBackXPath).Click();
            return new CombineSectionScreen(driver);
        }

        public SearchScreen InputTextInSearchField(string categoryName)
        {
            OpenSearchField();
            driver.FindElementByXPath(toolbarSearchTextXPath).SendKeys(categoryName);
            return new SearchScreen(driver);
        }

        public Header CleanSearchField()
        {
            driver.FindElementByXPath(toolbarSearchOffXPath).Click();
            return new Header(driver);
        }

        public Header CheckScreenTitle(string expectedScreenTitle)
        {
            Assert.AreEqual(expectedScreenTitle, GetScreenTitle());
            return this;
        }

    }
}
