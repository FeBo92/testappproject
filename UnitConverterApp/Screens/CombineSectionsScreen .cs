﻿using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;

namespace UnitConverterApp.Screens
{
    public class CombineSectionScreen : Header
    {
        public CombineSectionScreen(AppiumDriver<AndroidElement> driver)
            : base(driver)
        {

        }


        private string elementXPath = "//*[@text='category']";

        private AndroidElement FindCategory(string categoryName)
        {
            return driver.FindElementByXPath(elementXPath.Replace("category", categoryName));
        }
        public DownloadSpeedScreen GetDownloadSpeedScreen()
        {
            FindCategory("Download speed").Click();
            return new DownloadSpeedScreen(driver);
        }
        public FuelConsumptionCalculationScreen GetFuelConsumptionCalculationScreen()
        {
            FindCategory("Fuel consumption calculation").Click();
            return new FuelConsumptionCalculationScreen(driver);
        }

            public MovementSpeedScreen GetMovementSpeedScreen()
        {
            FindCategory("Moving speed").Click();
            return new MovementSpeedScreen(driver);
        }
        public OhmsLowScreen GetOhmsLawScreen()
        {
            driver.FindElementByXPath("(//*[@id='list_categories']/*/*/*/*[@id='name'])[5]").Click();
            return new OhmsLowScreen(driver);
        }

    }
}