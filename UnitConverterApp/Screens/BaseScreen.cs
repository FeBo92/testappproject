﻿using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;

namespace UnitConverterApp.Screens
{
    public abstract class BaseScreen
    {
        protected AppiumDriver<AndroidElement> driver;
        protected double elementWaitSeconds = 10;

        public BaseScreen(AppiumDriver<AndroidElement> driver)
        {
            this.driver = driver;
        }
    }
}
