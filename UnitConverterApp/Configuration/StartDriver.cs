﻿using OpenQA.Selenium.Appium.Android;
using System;

namespace UnitConverterApp.Configuration
{
    public class StartDriver
    {
        public static AndroidDriver<AndroidElement> GetDriver()
        {
            return new AndroidDriver<AndroidElement>(GetUri(), ConfigurationHelper.GetOptions());
        }

        private static Uri GetUri()
        {
            return new Uri("http://127.0.0.1:4723/wd/hub/");
        }

    }
}
