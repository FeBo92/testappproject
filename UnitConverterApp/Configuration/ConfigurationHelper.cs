﻿using OpenQA.Selenium.Appium;
using UnitConverterApp.Options;

namespace UnitConverterApp.Configuration
{
    public class ConfigurationHelper
    {

        public static AppiumOptions GetOptions()
        {
            AppiumOptions options = new AppiumOptions();
            options = GetDeviceOptions(options);
            options = GetApplicationOptions(options);
            return options;
        }

        private static AppiumOptions GetApplicationOptions(AppiumOptions options)
        {
            options.AddAdditionalCapability("appPackage", AppOptions.AppPackage);
            options.AddAdditionalCapability("appActivity", AppOptions.AppActivity);
            return options;
        }

        private static AppiumOptions GetDeviceOptions(AppiumOptions options)
        {
            options.AddAdditionalCapability("platformName", DeviceOptions.PlatformName);
            options.AddAdditionalCapability("deviceName", DeviceOptions.DeviceName);
            options.AddAdditionalCapability("platformVersion", DeviceOptions.PlatformVersion);
            return options;
        }
    }
}
