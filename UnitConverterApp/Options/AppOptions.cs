﻿namespace UnitConverterApp.Options
{
    public class AppOptions
    {
        public static string AppPackage { get; set; } = "com.kuzmin.konverter";
        public static string AppActivity { get; set; } = "com.kuzmin.konverter.ActivityMainMenu";
    }
}
