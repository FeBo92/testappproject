﻿namespace UnitConverterApp.Options
{
    public class DeviceOptions
    {
        public static string DeviceName { get; set; } = "Android";
        //public static string PlatformName { get; set; } = "28c7c4c2";
        public static string PlatformName { get; set; } = "3fc7ac7d";
        public static string PlatformVersion { get; set; } = "9";
    }
}
