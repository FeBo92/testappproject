﻿using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using UnitConverterApp.Screens;


namespace UnitConverterApp.Actions
{
    public class FieldsManager : BaseScreen
    {
        public FieldsManager(AppiumDriver<AndroidElement> driver)
         : base(driver)
        {

        }
        
        private string fieldXPath = "//*[@class='android.widget.TextView' and ./parent::" +
                                    "*[@class='android.widget.LinearLayout']" + 
                                    "and (./preceding-sibling::* | ./following-sibling::*)" +
                                    "[@class='android.widget.LinearLayout' and ./*[@text='fieldName']]]";
        private string currentFieldXpath = "//*[@class='android.widget.EditText']";

        private AndroidElement GetField(string fieldName)
        {
            return driver.FindElementByXPath(fieldXPath.Replace("fieldName", fieldName));
        }

        private AndroidElement InputTextField()
        {
            return driver.FindElementByXPath(currentFieldXpath);
        }

        private string ReadValueField(string fieldName)
        {
            return GetField(fieldName).Text;
        }

        public void InputValueField(string fieldName, string value)
        {
            GetField(fieldName).Click();
            InputTextField().SendKeys(value);
        }

        public void CheckValueField(string expectedValueField, string fieldName)
        {
            Assert.AreEqual(expectedValueField, ReadValueField(fieldName).Replace(" ", ""));
        }
    }
}
