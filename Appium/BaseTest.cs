﻿using Appium.Logs;
using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using System;
using UnitConverterApp.Configuration;
using UnitConverterApp.Screens;

namespace Appium
{

    public class BaseTest
    {
        protected AppiumDriver<AndroidElement> driver;
        protected MainScreen mainScreen;

        [SetUp]
        public void SetUp()
        {
            driver = StartDriver.GetDriver();
            mainScreen = new MainScreen(driver);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }

        [TearDown]
        public void TearDown()
        {
            LogsApp.Log();
            driver.CloseApp();
            driver.Quit();
        }

    }
}
