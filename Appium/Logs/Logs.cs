﻿using NLog;
using NUnit.Framework;

namespace Appium.Logs
{
    public class LogsApp
    {
        private static Logger logger = LogManager.GetLogger("logger");
        private static int counter = 1; 
        public static void Log()
        {
            
            var className = TestContext.CurrentContext.Test.ClassName;
            var methodName = TestContext.CurrentContext.Test.MethodName;
            var attributes = TestContext.CurrentContext.Test.Name;
            var arguments = TestContext.CurrentContext.Test.Arguments;
            var result = TestContext.CurrentContext.Result.Outcome;
            var message = TestContext.CurrentContext.Result.Message;

            logger.Debug(new string('-', 40));
            logger.Debug("Test number:{0}", counter++);
            logger.Debug("Class name: {0}", className);
            logger.Debug("Method name:{0}", methodName);
            logger.Debug("Input data: {0}", GetAttributes(attributes, methodName));
            logger.Debug("Arguments:  {0}", arguments);
            logger.Debug("Result:     {0}", result.Status);
            logger.Debug("Message:    {0}", message);
            logger.Debug(new string('-', 40));
        }


        private static string GetAttributes(string attributes, string methodName)
        {
            attributes = attributes.Replace(methodName, "");
            attributes = attributes.TrimStart('(').TrimEnd(')');
            return attributes;
        }
        
    }
}
