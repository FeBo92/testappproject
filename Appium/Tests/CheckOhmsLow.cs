﻿using NUnit.Framework;


namespace Appium.Tests
{
    [TestFixture]
    public class CheckOhmsLow : BaseTest
    {
        [TestCase("28", "2", "56")]
        [TestCase("200", "0.2", "40")]
        public void CheckOhmsLowTest(string value1,
                                     string value2,
                                     string output)
        {
            mainScreen
                      .GetCombineSectionScreen()
                      .GetOhmsLawScreen()
                      .InputValueField1(value1)
                      .InputValueField2(value2)
                      .CheckValueField3(output);
        }
    }
}
