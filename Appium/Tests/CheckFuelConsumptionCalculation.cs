﻿using NUnit.Framework;


namespace Appium.Tests
{
    [TestFixture]
    public class CheckFuelConsumptionCalculation : BaseTest
    {
        [TestCase("20", "100", "500")]
        [TestCase("300", "50", "16.6667")]
        public void CheckFuelConsumptionCalculationTest(string value1,
                                     string value2,
                                     string output)
        {
            mainScreen
                      .GetCombineSectionScreen()
                      .GetFuelConsumptionCalculationScreen()
                      .InputValueField1(value1)
                      .InputValueField2(value2)
                      .CheckValueField3(output);
        }
    }
}
