﻿using NUnit.Framework;
using UnitConverterApp.Screens;

namespace Appium.Tests
{
    [TestFixture]
    class CheckAddToFavorite : BaseTest
    {

        [TestCase("Time")]
        public void ChechAddToFavoriteTest(string categoryName)
        {
            mainScreen
                      .GetMyFavoriteScreen()
                      .GetAllConverors()
                      .ClickOnCategory(categoryName)
                      .GetToolbarMenu()
                      .AddToFavorite()
                      .ClickBack()
                      .ClickBack()
                      .CheckConvertor(categoryName, categoryName)
                      .RemoveFromFavorite(categoryName);
        }
       
    }
}
