﻿using NUnit.Framework;

namespace Appium.Tests
{
    [TestFixture]
    public class CheckOpenNeedScreenFromMainTest : BaseTest
    {
        [TestCase("Main sections", "Main sections")]
        [TestCase("Combined sections", "Combined sections")]
        public void CheckOpenNeedScreenFromMain(string expectedScreenTitle, string screenName)
        {
            mainScreen
                      .GetNeedScreen(screenName)
                      .CheckScreenTitle(expectedScreenTitle);
        }
    }
}
