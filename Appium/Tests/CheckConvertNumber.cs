﻿using NUnit.Framework;


namespace Appium.Tests
{
    [TestFixture]
    public class CheckConvertNumber : BaseTest
    {
        [TestCase("Binary", "1010", "Decimal", "10")]
        [TestCase("Decimal", "10", "Roman", "X")]
        public void CheckConvertNumberTest(string inputFieldName,
                                     string inputValue,
                                     string outputFieldName,
                                     string expectrdValue)
        {
            mainScreen
                      .GetMainSectionsScreen()
                      .GetNumberScreen()
                      .InputValueField(inputFieldName, inputValue)
                      .CheckValueField(expectrdValue, outputFieldName);
        }
    }
}
