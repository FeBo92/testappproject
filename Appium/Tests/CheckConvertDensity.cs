﻿using NUnit.Framework;


namespace Appium.Tests
{
    [TestFixture]
    public class CheckConvertDensity : BaseTest
    {
        [TestCase("Gram/liter", "749", "Ounce", "100")]
        [TestCase("Ounce", "100", "Gram/liter", "749")]
        public void CheckConvertDensityTest(string inputFieldName,
                                     string inputValue,
                                     string outputFieldName,
                                     string expectrdValue)
        {
            mainScreen
                      .GetMainSectionsScreen()
                      .GetDensityScreen()
                      .InputValueField(inputFieldName, inputValue)
                      .CheckValueField(expectrdValue, outputFieldName);
        }
    }
}
