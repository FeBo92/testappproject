﻿using NUnit.Framework;

namespace Appium.Tests
{
    [TestFixture]
    class CheckConvertPressureTest : BaseTest
    {
        [TestCase("Pascal", "15", "Hectopascal", "0.15")]
        [TestCase("Hectopascal", "0.15", "Pascal", "15")]
        public void CheckConvertPressure(string inputFieldName,
                                     string inputValue,
                                     string outputFieldName,
                                     string expectrdValue)
        {
            mainScreen
                      .GetMainSectionsScreen()
                      .GetPreassureScreen()
                      .InputValueField(inputFieldName, inputValue)
                      .CheckValueField(expectrdValue, outputFieldName);
        }
    }
}
