﻿using NUnit.Framework;


namespace Appium.Tests
{
    [TestFixture]
    public class CheckConvertTemperatureTest : BaseTest
    {
        [TestCase("Celsius", "100", "Fahrenheit", "212")]
        [TestCase("Fahrenheit", "212", "Celsius", "100")]
        public void CheckConvertTemperature(string inputFieldName,
                                     string inputValue,
                                     string outputFieldName,
                                     string expectrdValue)
        {
            mainScreen
                      .GetMainSectionsScreen()
                      .GetTemperatureScreen()
                      .InputValueField(inputFieldName, inputValue)
                      .CheckValueField(expectrdValue, outputFieldName);
        }
    }
}