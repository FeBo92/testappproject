﻿using NUnit.Framework;


namespace Appium.Tests
{
    [TestFixture]
    public class CheckConvertLengthTest : BaseTest
    {
        [TestCase("Micrometer","50", "Millimeter", "0.05")]
        [TestCase("Millimeter", "0.05", "Micrometer", "50")]
        public void CheckConvertLength(string inputFieldName, 
                                     string inputValue, 
                                     string outputFieldName, 
                                     string expectrdValue)
        {
            mainScreen
                      .GetMainSectionsScreen()
                      .GetLengthScreen()
                      .InputValueField(inputFieldName, inputValue)
                      .CheckValueField(expectrdValue,outputFieldName);
        }
    }
}