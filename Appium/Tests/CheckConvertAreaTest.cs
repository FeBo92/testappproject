﻿using NUnit.Framework;

namespace Appium.Tests
{
    [TestFixture]
    class CheckConvertAreaTest : BaseTest
    {
        [TestCase("Acre", "15", "Hectare", "6.0703")]
        [TestCase("Hectare", "6.0703", "Acre", "15")]
        public void CheckConvertArea(string inputFieldName,
                                            string inputValue,
                                            string outputFieldName,
                                            string expectrdValue)
        {
            mainScreen
                      .GetMainSectionsScreen()
                      .GetSquareScreen()
                      .InputValueField(inputFieldName, inputValue)
                      .CheckValueField(expectrdValue, outputFieldName);
        }
    }
}

