﻿using NUnit.Framework;

namespace Appium.Tests
{
    [TestFixture]
    class CheckBackToMainSectionsScreenTest : BaseTest
    {
        [TestCase("Main sections")]
        public void CheckBackToMainSectionsScreen(string expectedScreenTitle)
        {
            mainScreen
                      .GetMainSectionsScreen()
                      .GetLengthScreen()
                      .ClickBack()
                      .CheckScreenTitle(expectedScreenTitle);
        }
    }
}
