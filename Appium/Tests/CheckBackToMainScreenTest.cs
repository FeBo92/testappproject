﻿using NUnit.Framework;

namespace Appium.Tests
{
    [TestFixture]
    class CheckBackToMainScreenTest : BaseTest
    {
        [TestCase("Main sections", "Сonverter v10")]
        [TestCase("Combined sections", "Сonverter v10")]
        public void CheckBackToMainScreen(string screenName, string expectedScreenTitle)
        {
            mainScreen
                      .GetNeedScreen(screenName)
                      .ClickBack()
                      .CheckScreenTitle(expectedScreenTitle);
        }
    }
}
