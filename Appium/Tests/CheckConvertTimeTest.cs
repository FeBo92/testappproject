﻿using NUnit.Framework;

namespace Appium.Tests
{
    [TestFixture]
    class CheckConvertTimeTest : BaseTest
    {
        [TestCase("Nanosecond", "25", "Microsecond", "0.025")]
        [TestCase("Microsecond", "0.025", "Nanosecond", "25")]
        public void CheckConvertTime(string inputFieldName,
                                           string inputValue,
                                           string outputFieldName,
                                           string expectrdValue)
        {
            mainScreen
                      .GetMainSectionsScreen()
                      .GetTimeScreen()
                      .InputValueField(inputFieldName, inputValue)
                      .CheckValueField(expectrdValue, outputFieldName);
        }
    }
}
