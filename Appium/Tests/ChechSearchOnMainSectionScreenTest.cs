﻿using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

namespace Appium.Tests
{
    [TestFixture]
    public class ChechSearchOnMainSectionScreenTest : BaseTest
    {

        [Test, TestCaseSource("TestData")]
        public void ChechSearchOnMainSectionScreen(string categoryName)
        {
           
            mainScreen
                      .GetMainSectionsScreen()
                      .InputTextInSearchField(categoryName)
                      .CheckFoundCategory(categoryName);
        }
        private static IEnumerable<object[]> TestData()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            StreamReader textStreamReader = new StreamReader
                (assembly.GetManifestResourceStream("Appium.Tests.ChechSearchOnMainSectionScreenTest.xml"));
            var doc = XDocument.Load(textStreamReader);
            return
                from vars in doc.Descendants("testData")
                let value = vars.Attribute("value").Value
                select new object[] { value };
        }

    }
}
