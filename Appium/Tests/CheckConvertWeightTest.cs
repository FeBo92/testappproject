﻿using NUnit.Framework;

namespace Appium.Tests
{
    [TestFixture]
    class CheckConvertWeightTest : BaseTest
    {
        [TestCase("Ounce", "50", "Pound", "3.125")]
        [TestCase("Pound", "3.125", "Ounce", "50")]
        public void CheckConvertWeight(string inputFieldName,
                                       string inputValue,
                                       string outputFieldName,
                                       string expectrdValue)
        {
            mainScreen
                      .GetMainSectionsScreen()
                      .GetWeightScreen()
                      .InputValueField(inputFieldName, inputValue)
                      .CheckValueField(expectrdValue, outputFieldName);
        }
    }
}
