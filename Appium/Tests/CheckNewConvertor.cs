﻿using NUnit.Framework;
using UnitConverterApp.Screens;

namespace Appium.Tests
{
    [TestFixture]
    public class CheckNewConvertor : BaseTest
    {
        [TestCase("Binary", "Whole", "Half", "1", "2")]
        public void CheckNewConvertorTest(string convertorName,
                                     string unitName1,
                                     string unitName2,
                                     string unitValue1,
                                     string unitValue2)
        {
            mainScreen
                      .GetMyConvertorsScreen()
                      .MakeConvertor()
                      .SetConvertorName(convertorName)
                      .AddFields()
                      .SetUnitName1(unitName1)
                      .SetUnitName2(unitName2)
                      .SetUnitValue1(unitValue1)
                      .SetUnitValue2(unitValue2)
                      .SaveFields()
                      .SaveConvertor()
                      .OpenConvertor(convertorName)
                      .InputValueField(unitName1, unitValue1)
                      .CheckValueField(unitValue2, unitName2);

        }
        [TearDown]
        public void Clean(){
            new MyConvertorsScreen(driver).DeleteConvertor();
                
        }
    }
  
}