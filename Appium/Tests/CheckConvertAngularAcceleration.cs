﻿using NUnit.Framework;


namespace Appium.Tests
{
    [TestFixture]
    public class CheckConvertAngularAcceleration : BaseTest
    {
        [TestCase("Radians per second per second", "1.008", "Radians per minute per minute", "3628.8")]
        [TestCase("Radians per minute per minute", "3600", "Radians per second per second", "1.008")]
        public void CheckConvertAngularAccelerationTest(string inputFieldName,
                                     string inputValue,
                                     string outputFieldName,
                                     string expectrdValue)
        {
            mainScreen
                      .GetMainSectionsScreen()
                      .GetAngularAccelerationScreen()
                      .InputValueField(inputFieldName, inputValue)
                      .CheckValueField(expectrdValue, outputFieldName);
        }
    }
}
