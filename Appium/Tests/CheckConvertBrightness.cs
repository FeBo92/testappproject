﻿using NUnit.Framework;


namespace Appium.Tests
{
    [TestFixture]
    public class CheckBrightnessNumber : BaseTest
    {
        [TestCase("Candela per square metre", "3183.1", "Lambert", "1")]
        [TestCase("Lambert", "1", "Candela per square metre", "3183.0989")]
        public void CheckConvertBrightnessTest(string inputFieldName,
                                     string inputValue,
                                     string outputFieldName,
                                     string expectrdValue)
        {
            mainScreen
                      .GetMainSectionsScreen()
                      .GetBrightnessScreen()
                      .InputValueField(inputFieldName, inputValue)
                      .CheckValueField(expectrdValue, outputFieldName);
        }
    }
}
