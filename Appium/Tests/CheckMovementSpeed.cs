﻿using NUnit.Framework;


namespace Appium.Tests
{
    [TestFixture]
    public class CheckMovementSpeed : BaseTest
    {
        [TestCase("70", "1", "70")]
        [TestCase("20000", "25", "800")]
        public void CheckMovementSpeedTest(string value1,
                                     string value2,
                                     string output)
        {
            mainScreen
                      .GetCombineSectionScreen()
                      .GetMovementSpeedScreen()
                      .InputValueField1(value1)
                      .InputValueField2(value2)
                      .CheckValueField3(output);
        }
    }
}
