﻿using NUnit.Framework;


namespace Appium.Tests
{
    [TestFixture]
    public class CheckDownloadSpeed : BaseTest
    {
        [TestCase("1", "1", "1")]
        [TestCase("60", "3600", "60")]
        public void CheckDownloadSpeedTest(string value1,
                                     string value2,
                                     string output)
        {
            mainScreen
                      .GetCombineSectionScreen()
                      .GetDownloadSpeedScreen()
                      .InputValueField1(value1)
                      .InputValueField2(value2)
                      .CheckValueField3(output);
        }
    }
}
