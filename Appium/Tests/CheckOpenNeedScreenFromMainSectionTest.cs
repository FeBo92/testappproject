﻿using NUnit.Framework;

namespace Appium.Tests
{
    [TestFixture]
    public class CheckOpenNeedScreenFromMainSectionTest : BaseTest
    {
        [TestCase("Time", "Time")]
        [TestCase("Length", "Length")]
        [TestCase("Weight", "Weight")]
        [TestCase("Pressure", "Pressure")]
        [TestCase("Area", "Area")]
        public void CheckOpenNeedScreenFromMainSection(string categoryName, string expectedCategory)
        {
            mainScreen
                      .GetMainSectionsScreen()
                      .OpenCategoryInMainSections(categoryName)
                      .CheckScreenTitle(expectedCategory);
        }
    }
}
